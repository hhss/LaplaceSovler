#!/usr/bin/bash
cython3 CLaplace5.pyx
gcc -c -fPIC -I/usr/include/python3.4 CLaplace5.c 
gcc -o CLaplace5.so CLaplace5.o -shared
rm *.o
