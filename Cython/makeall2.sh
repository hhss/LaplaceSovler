#!/usr/bin/bash
cython3 CLaplace6.pyx 
gcc -c -fPIC -I/usr/include/python3.4 CLaplace6.c 
gcc -o CLaplace6.so CLaplace6.o -shared
rm *.o
