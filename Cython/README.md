#用Cython改写没有变快, 这是为什么呢?
谁能告诉我!!!之前有两个项目, 比较简单, 同样用到了numpy库, 但是用Cython改写后速度提升了30倍和47倍, 为什么这个项目没有啊?!

#CLaplace5.pyx
CLaplace5.pyx是使用Python写的最后一个版本, 直接从../Python/Laplace5.py文件夹中拷贝过来, 删除主函数并将画图函数和相关库移至run-demo1.py.

#CLaplace5.c
CLaplace5.c是对CLaplace5.pyx直接使用cython3生成的C版本.

#CLaplace5.so
CLaplace5.so是用makeall1.sh编译得到的.

#run-demo1.py
导入CLaplace5.so运行不同网格数时的程序, 记录时间.

#using_cython_without_modifing_python_code
run-demo1.py生成的文件, 记录划分网格数与时间的关系

#CLaplace6.pyx
Laplace6.pyx是在CLaplace5.pyx的基础上添加了变量类型声明.

#CLaplace6.c
CLaplace6.c是对CLaplace6.pyx使用cython3生成的C版本.

#CLaplace6.so
CLaplace6.so是用makeall2.sh编译得到的.

#run-demo2.py
导入CLaplace6.so运行不同网格数时的程序, 记录时间.

#using_cython_and_modifing_python_code
run-demo2.py生成的文件, 记录划分网格数与时间的关系
