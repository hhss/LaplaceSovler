import numpy as np
from matplotlib.pyplot import plot, savefig, title, legend, ylim, cla, xlabel, ylabel, show
import sys
with open(sys.argv[1] ,"r") as Origin_Performance, open(sys.argv[2] ,"r") as Optimise_Performance: 
	Num_Lines_Origin = sum(1 for line in Origin_Performance)
	Num_Lines_Optimise = sum(1 for line in Optimise_Performance)
	#Num_Lines_Optimise_Feat = sum(1 for line in Optimise_Feat_Performance)
Figure_Name = "Performance Comparing between Two Optimised Algorithm"
with open(sys.argv[1] ,"r") as Origin_Performance, open(sys.argv[2] ,"r") as Optimise_Performance: 
	Number_of_Node_Origin = np.zeros(Num_Lines_Origin)
	Number_of_Node_Optimise = np.zeros(Num_Lines_Optimise)
	#Number_of_Node_Optimise_Feat = np.zeros(Num_Lines_Optimise_Feat)
	Time_of_Origin = np.zeros(Num_Lines_Origin)
	Time_of_Optimise = np.zeros(Num_Lines_Optimise)
	#Time_of_Optimise_Feat = np.zeros(Num_Lines_Optimise_Feat)
	for (i, j, Column_Origin, Column_Optimise) in zip(range(Num_Lines_Origin), range(Num_Lines_Optimise), Origin_Performance, Optimise_Performance):
		Number_of_Node_Origin[i] = int(Column_Origin.split()[0])**2
		#print(Column_Origin.split()[0])
		Number_of_Node_Optimise[j] = int(Column_Optimise.split()[0])**2
		#Number_of_Node_Optimise_Feat[k] = int(Column_Optimise_Feat.split()[0])**2
		Time_of_Origin[i] = Column_Origin.split()[1]
		Time_of_Optimise[j] = Column_Optimise.split()[1]
		#Time_of_Optimise_Feat[k] = Column_Optimise_Feat.split()[1]
#print(Number_of_Node_Origin)
#print(Time_of_Origin)
title("$\mathrm{"+Figure_Name.replace(" ","\,")+"}$")
xlabel("$\mathrm{Number\,of\,Node}$")
ylabel("$\mathrm{Calculate\, Time\,(second)}$")
ylim(0, max(Time_of_Origin)*1.1)
plot(Number_of_Node_Origin, Time_of_Origin, "g--", linewidth=2, label="$\mathrm{Matrix\,Multiplication\,Optimised}$")
plot(Number_of_Node_Optimise, Time_of_Optimise, "r-", linewidth=2, label="$\mathrm{Matrix\,Multiplication\,and\,Iteration\,Optimised}$")
#plot(Number_of_Node_Optimise_Feat, Time_of_Optimise_Feat, "r-", linewidth=2, label="$\mathrm{Optimised-Again\,Algorithm}$")
legend(loc="upper left")
savefig(Figure_Name.replace(" ", "_")+".eps")
show()
